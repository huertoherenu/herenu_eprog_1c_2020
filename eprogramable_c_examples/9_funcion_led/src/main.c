/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/

enum {
	ON,
	OFF,
	TOGGLE
} mode;

enum {
	LED1 = 1,
	LED2,
	LED3
}led_t;

/*==================[internal functions declaration]=========================*/


typedef struct{
		uint8_t n_led;
		uint8_t n_ciclos;
		uint8_t periodo;
		uint8_t mode;
}leds_t;

uint8_t i;
uint8_t j;

void ManejoLeds (leds_t *my_leds){

	switch (my_leds->mode){	//Selección del modo
				case ON:
					switch( my_leds->n_led ){
						case LED1:
							printf("Se enciende el led: %d \r\n", my_leds->n_led);
						break;

						case LED2:
							printf("Se enciende el led: %d \r\n", my_leds->n_led);
						break;

						case LED3:
							printf("Se enciende el led: %d \r\n", my_leds->n_led);
						break;
					}		//Fin switch n_leds ON

				case OFF:
					switch( my_leds->n_led ){
					case LED1:
							printf("Se apaga el led: %d \r\n", my_leds->n_led);
					break;

					case LED2:
							printf("Se apaga el led: %d \r\n", my_leds->n_led);
					break;

					case LED3:
							printf("Se apaga el led: %d \r\n", my_leds->n_led);
					break;
					}		//Fin switch n_leds ON

				case TOGGLE:

					for (i=0; i<my_leds->n_ciclos; i++){
						switch(my_leds->n_led){
												case LED1:
													printf("Se togglea el led: %d \r\n", my_leds->n_led);
												break;

												case LED2:
													printf("Se togglea el led: %d \r\n", my_leds->n_led);
												break;

												case LED3:
													printf("Se toggleael led: %d \r\n", my_leds->n_led);
												break;
											}		//Fin switch n_leds TOGGLE

						for(j=0; j<my_leds->periodo; j++){ };

						}		//Fin for i

	}		//Fin switch mode
}// Fin Function LedControl


int main(void)
{
	 leds_t mi_led = {
			   LED2,
			   5,
			   10,
			   TOGGLE
	   };

	   ManejoLeds (&mi_led);

	return 0;
}

/*==================[end of file]============================================*/

