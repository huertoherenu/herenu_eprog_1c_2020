/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

typedef struct{
uint8_t port;			/*!< GPIO port number */
uint8_t pin; 			/*!< GPIO pin number */
uint8_t dir; 			/*!< GPIO direction ‘0’ IN; ‘1 ’ OUT */
} gpioConf_t;


void display(uint8_t BCD, gpioConf_t *configuracion){
 	/*Puerto 1.4*/
	if (BCD & 1){
 		printf ("En ALTO b0 -> puerto %d.%d \r\n", configuracion[0].port, configuracion[0].pin);
 	}
 	else{
 		printf ("En BAJO b0 -> puerto %d.%d \r\n", configuracion[0].port, configuracion[0].pin);
 	}
	/*Puerto 1.5*/
	if (BCD & 2){
 		printf ("En ALTO b1 -> puerto %d.%d \r\n", configuracion[1].port, configuracion[1].pin);
 	}
 	else{
 		printf ("En BAJO b1 -> puerto %d.%d \r\n", configuracion[1].port, configuracion[1].pin);
 	}
	/*Puerto 1.6*/
	if (BCD & 4){
 		printf ("En ALTO b2 -> puerto %d.%d \r\n", configuracion[2].port, configuracion[2].pin);
 	}
 	else{
 		printf ("En BAJO b2 -> puerto %d.%d \r\n", configuracion[2].port, configuracion[2].pin);
 	}
	/*Puerto 2.14*/
	if (BCD & 8){
 		printf ("En ALTO b3 -> puerto %d.%d \r\n", configuracion[3].port, configuracion[3].pin);
 	}
 	else{
 		printf ("En BAJO b3 -> puerto %d.%d \r\n", configuracion[3].port, configuracion[3].pin);
 	}
};		//Fin Display

/*Pasar número a BCD*/
uint8_t i;
int8_t ToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number ){
	for ( i=0 ; i<digits ; i++ ){
		bcd_number[i] = data % 10;
		data = data / 10;
	}
};		//Fin función BinaryToBCD


int main(void)
{
	gpioConf_t GPIO [4];		// Vector de configuración de los puertos

// Configuración de GPIO
	GPIO[0].port= 1;
	GPIO[0].pin	= 4;
	GPIO[0].dir	= 1;

	GPIO[1].port= 1;
	GPIO[1].pin	= 5;
	GPIO[1].dir	= 1;

	GPIO[2].port= 1;
	GPIO[2].pin	= 6;
	GPIO[2].dir	= 1;

	GPIO[3].port= 2;
	GPIO[3].pin	= 14;
	GPIO[3].dir	= 1;

	uint32_t data = 1234;		//Número cargado
	uint8_t digitos, i ; 		//Cantidad de bcd necesarios y variable aux
	uint32_t numeroBCD[4];
	digitos = 4;

	ToBcd(data, digitos, numeroBCD);		//Se convierte el número a bcd

	for(i=0; i<digitos;i++){
		display(numeroBCD[i], GPIO);
	}
	return 0;
}

/*==================[end of file]============================================*/

