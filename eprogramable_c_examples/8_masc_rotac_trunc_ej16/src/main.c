/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"

#include <stdint.h>
#include <stdio.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main(void)
{

	uint32_t variable = 0x01020304;
	uint8_t var1, var2, var3, var4;

	var1 = (uint8_t) variable;
	var2 = (uint8_t) (variable>>8*1);
	var3 = (uint8_t) (variable>>8*2);
	var4 = (uint8_t) (variable>>8*3);


/*mostrar resultado*/
		printf("variable  = %d \r\n",variable );
		printf("variable1  = %d \r\n",var1 );
		printf("variable2  = %d \r\n",var2 );
		printf("variable3  = %d \r\n",var3 );
		printf("variable4  = %d \r\n",var4 );

/*con UNION*/
		union Union{
				uint32_t entero;
				struct cada_byte{
						uint8_t byte1;
						uint8_t byte2;
						uint8_t byte3;
						uint8_t byte4;
					} ;
					uint32_t todos_los_bytes;
				}

		union Union valor;

		valor.entero = variable;

	/*mostrar resultado*/
	printf("variable  = %d \r\n",valor.entero );
	printf("variable1  = %d \r\n",valor.todos_los_bytes.byte1 );
	printf("variable2  = %d \r\n",valor.todos_los_bytes.byte2 );
	printf("variable3  = %d \r\n",valor.todos_los_bytes.byte3 );
	printf("variable4  = %d \r\n",valor.todos_los_bytes.byte4 );



	//revisar con el de teoria


	valor.entero=variable;

/*mostrar resultado*/

	return 0;
}

/*==================[end of file]============================================*/

