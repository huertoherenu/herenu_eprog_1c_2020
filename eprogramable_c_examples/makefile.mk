########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1

#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

#PROYECTO_ACTIVO = 4_cambio_valor_ej7
#NOMBRE_EJECUTABLE = cambio_variable.exe

#PROYECTO_ACTIVO = 5_verificar_valor_ej9
#NOMBRE_EJECUTABLE = verificar_valor.exe

#PROYECTO_ACTIVO = 6_puntero_ej12
#NOMBRE_EJECUTABLE = puntero.exe

#PROYECTO_ACTIVO = 7_struct_ej14
#NOMBRE_EJECUTABLE = estructura.exe

#PROYECTO_ACTIVO = 8_masc_rotac_trunc_ej16
#NOMBRE_EJECUTABLE = union.exe

#PROYECTO_ACTIVO = 9_funcion_led
#NOMBRE_EJECUTABLE = LED.exe

#PROYECTO_ACTIVO = 10_binario_bcd
#NOMBRE_EJECUTABLE = BinarioToBCD.exe

#PROYECTO_ACTIVO = 11_Conf_puerto
#NOMBRE_EJECUTABLE = Conf_Puerto.exe

PROYECTO_ACTIVO = 12_promedio
NOMBRE_EJECUTABLE = Promedio.exe

