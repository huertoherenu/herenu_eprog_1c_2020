/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

#include "main.h"

#include <stdint.h>
#include <stdio.h>
#include <string.h>


/*==================[macros and definitions]=================================*/

typedef	struct {
		uint8_t nombre[12];
		uint8_t apellido[20];
		uint8_t edad;
		}Alumno;


/*==================[internal functions declaration]=========================*/

int main(void)
{
	Alumno Alu;

	strcpy(Alu.nombre, "Huerto");
	printf("Nombre del alumno : %s \r\n",Alu.nombre);
	strcpy(Alu.apellido, "Hereñu");
	printf("Apellido del alumno: %s \r\n", Alu.apellido);
	Alu.edad = 27;
	printf("Edad del alumno: %d \r\n",Alu.edad);


//CON PUNTERO

	Alumno *B;
	B = &Alu;

	strcpy(B->nombre, "Ivana");
	printf("Nombre del alumno 2: %s \r\n",B->nombre);
	strcpy(B->apellido, "Obregon");
	printf("Apellido del alumno 2: %s \r\n",B->apellido);
	B->edad = 28;
	printf("Edad del alumno 2 %d \r\n",B->edad);


	return 0;
}

/*==================[end of file]============================================*/

