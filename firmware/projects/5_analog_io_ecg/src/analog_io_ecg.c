/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/analog_io_ecg.h"       /* <= own header */
#include "systemclock.h"
#include "timer.h"
#include "delay.h"
#include "uart.h"
#include "analog_io.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define ON  1
#define OFF  0
#define BUFFER_SIZE 231											//cantidad de muestras ECG

const char ecg[BUFFER_SIZE]={									//vector muestras ECG
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

/*==================[internal data definition]===============================*/
uint8_t timer_end = OFF;
uint8_t convert_end = OFF;
uint8_t timer_DAC = OFF;
uint16_t dato_ecg = 0;
uint8_t dato;

/*variables para procesamiento*/
uint8_t FILT = OFF;
uint16_t dato_salida = 0;
uint16_t filtrado_anterior = 1;
uint8_t fc = 20;												//frecuencia de corte
float dt = 0.002;
float alpha;
uint16_t salida_filtrada = 0;
float RC;

/*==================[internal functions declaration]=========================*/

/* @brief indica la interrupcion para la conversión analógica
 * @param[in] No Parameter
 */
void DoTimer(void);

/* @brief indica la interrupcion
 * @param[in] No Parameter
 */
void DoUart(void);

/* @brief indica la interrupcion para el ADC
 * @param[in] No Parameter
 */
void DoADC(void);

 /* @brief indica la interrupcion para el DAC
  * @param[in] No Parameter
  */
void DoDAC(void);

 /* @brief Filtro pasa bajo para procesamiento de la señal
   * @param[in] No Parameter
   */
uint16_t Filtro_PB(uint16_t nuevo_a_filtrar, uint16_t anterior_filtrado);

 /*interrupciones de teclas*/

 /* @brief Activa el filtrado
    * @param[in] No Parameter
    */
void Tecla_1(void);

 /* @brief Desactiva el filtrado
    * @param[in] No Parameter
    */
void Tecla_2(void);

 /* @brief Baja la frecuencia de corte
    * @param[in] No Parameter
    */
void Tecla_3(void);

 /* @brief Sube la frecuencia de corte
    * @param[in] No Parameter
    */
void Tecla_4(void);

/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A, 2, &DoTimer};
timer_config my_timer2 = {TIMER_B, 4, &DoDAC};
serial_config my_uart = {SERIAL_PORT_PC, 115200, &DoUart};
analog_input_config my_ADC = {CH1, AINPUTS_SINGLE_READ, &DoADC};

/*==================[external functions definition]==========================*/

void DoTimer(void)
{
	timer_end = ON;
}

void DoDAC(void)
{
	timer_DAC = ON;
}

void DoUart(void)
{
	UartReadByte(my_uart.port, &dato);
}

void DoADC(void)		//Analógico-Digital
{
	AnalogInputRead(my_ADC.input, &dato_ecg);
	convert_end = ON;
}

void Tecla_1(void)
{
	FILT = ON;
}

void Tecla_2(void)
{
	FILT = OFF;
}

void Tecla_3(void)
{
	fc -= 1;				//fc disminuye en 1 cada vez que se toca la tecla 3
}

void Tecla_4(void)
{
	fc += 1;				////fc aumenta en 1 cada vez que se toca la tecla 4
}

/*funcion para procesamiento de señales*/

uint16_t Filtro_PB(uint16_t nuevo_a_filtrar, uint16_t anterior_filtrado)
{
	RC = 1/(2*3.14*fc);
	alpha = dt/(RC+dt);
	salida_filtrada = anterior_filtrado + alpha * (nuevo_a_filtrar - anterior_filtrado);

	return salida_filtrada;
}

void SysInit(void)					//inicializaciones
{
	SystemClockInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1, Tecla_1);
	SwitchActivInt(SWITCH_2, Tecla_2);
	SwitchActivInt(SWITCH_3, Tecla_3);
	SwitchActivInt(SWITCH_4, Tecla_4);
	TimerInit(&my_timer);
	TimerInit(&my_timer2);
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);
	UartInit(&my_uart);
	AnalogInputInit(&my_ADC);
	AnalogOutputInit();
}

int main(void)
{
	SysInit();
	uint8_t index = 0;

	while(1)
    {
    	if(timer_end == ON)
    	{
    		AnalogStartConvertion();
    		timer_end = OFF;
    	}

    	if (convert_end == ON)
    	{
    		if(FILT == ON)
    		{

   				UartSendString(my_uart.port, UartItoa(dato_ecg, 10));
   				UartSendString(my_uart.port, ",");

   				dato_salida = Filtro_PB(dato_ecg, filtrado_anterior);

   				UartSendString(my_uart.port, UartItoa(dato_salida, 10));
   				UartSendString(my_uart.port, "\r");

   				filtrado_anterior = dato_salida;
    		}

    		if (FILT == OFF)
    		{
	    		UartSendString(my_uart.port, UartItoa(dato_ecg, 10));
    		    UartSendString(my_uart.port, "\r");
   			}

    		convert_end = OFF;
    	}

    	if(timer_DAC == ON)
    	{
    			AnalogOutputWrite(ecg[index]);
    			index++;
    			if(index == BUFFER_SIZE)
    			{
    				index = 0;
    			}
    			timer_DAC = OFF;
    	}
    }
 }


/*==================[end of file]============================================*/

