/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es:
 * Hereñu Huerto - huerto.herneu@gmail.com
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Ejercicio1.h"

#include "hc_sr4.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/* @brief Mide el volumen del vaso
 * @param[in] No Parameter
 */
void Indicate_Measuring ();

/* @brief indica la interrupcion para periferico UART
 * @param[in] No Parameter
 */
void DoUart();

/*==================[external data definition]===============================*/

gpio_t echo = T_FIL2;			//(pin37)
gpio_t trigger = T_FIL3;		//(pin35)

int16_t volumen = 0;

timer_config my_timer = {TIMER_A, 100, &Indicate_Measuring};		//TIMER_A en [mseg] , envia dato 10 veces por segundo
serial_config my_uart = {SERIAL_PORT_PC, 115200, &DoUart};

uint8_t dato_PC;

/*==================[external functions definition]==========================*/

void DoUart()
{
	UartReadByte(my_uart.port, &dato_PC);
}

void Indicate_Measuring ()
{
	volumen = HcSr04Readvolumen();									//mide el volumen a partir de la distancia sensada
	UartSendString(my_uart.port, UartItoa(volumen, 10));			//se muestra con el siguiente formato: xx + “ cm3”
	UartSendString(my_uart.port, " cm3" );
	UartSendString(my_uart.port, "\r\n");
}

	/*INICIALIZACIONES*/
void SysInit(void)
{
	SystemClockInit();
	HcSr04Init(echo, trigger);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_uart);
}


int main(void)
{
		SysInit();

		while (1){
					}
	return 0;
}

/*==================[end of file]============================================*/
