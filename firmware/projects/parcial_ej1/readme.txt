parcial_ej1

Con esta aplicación se mide el volumen de agua que hay en un vaso con parametros predeterminados (diámetro y altura)
a traves de la función HcSr04Readvolumen del driver hc_sr4, y se visualiza el mismo a traves del periférico UART 
con el siguiente formato xx + “ cm3”.
Los datos se envian 10 por segundo. 
