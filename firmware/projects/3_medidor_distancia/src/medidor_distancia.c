/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/medidor_distancia.h"
#include "systemclock.h"
#include "hc_sr4.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/* @brief Indica si se presionó la tecla 1 de la EDU-CIAA
 * @param[in] No Parameter
 */
void Tecla_1 ();

/* @brief Indica si se presionó la tecla 2 de la EDU-CIAA
 * @param[in] No Parameter
 */
void Tecla_2 ();

/* @brief Mide y muestra la distancia sensada por el sensor HC-SR4
 * @param[in] No Parameter
 */
void Indicate_Measuring ();

/* @brief indica la interrupcion por teclado de PC
 * @param[in] No Parameter
 */
void DoUart();

/*==================[external data definition]===============================*/

bool control = true;			//para medir
bool hold = false;				//para mostrar resultado

gpio_t echo = T_FIL2;			//(pin37)
gpio_t trigger = T_FIL3;		//(pin35)

int16_t distancia = 0;

timer_config my_timer = {TIMER_A, 1000, &Indicate_Measuring};		//TIMER_A en [mseg]
serial_config my_uart = {SERIAL_PORT_PC, 115200, &DoUart};

uint8_t dato_PC;

/*==================[external functions definition]==========================*/

void Tecla_1 ()
	{control =! control;}

void Tecla_2 ()
	{hold =! hold;}

void DoUart(){
	UartReadByte(my_uart.port, &dato_PC);

	if (dato_PC == 'O')
		{control =! control;}

	else if (dato_PC == 'H')
		{hold =! hold;}
}

void Indicate_Measuring ()
{
	if(control == false){ 														//medición activada
			distancia = HcSr04ReadDistanceCentimeters();}

	if(hold == true){
			UartSendString(my_uart.port, UartItoa(distancia, 10));
			UartSendString(my_uart.port, " cm" );
			UartSendString(my_uart.port, "\r\n");}
}

	/*INICIALIZACIONES*/
void SysInit(void)
{
	SystemClockInit();
	HcSr04Init(echo, trigger);
	LedsInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1, Tecla_1);
	SwitchActivInt(SWITCH_2, Tecla_2);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_uart);
}


int main(void)
{
		SysInit();

		while (1){
					}
	return 0;
}

/*==================[end of file]============================================*/
