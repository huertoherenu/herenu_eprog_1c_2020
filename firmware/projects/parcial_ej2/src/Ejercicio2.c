/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es:
 * Hereñu, Huerto huerto.herenu@gmail.com
 *
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Ejercicio2.h"       /* <= own header */
#include "systemclock.h"
#include "timer.h"
#include "delay.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
#define ON  1
#define OFF  0
#define ADC_BUFF_SIZE 40		//para calcular el promedio cada 1 seg

/*==================[internal data definition]===============================*/
uint8_t timer_end = OFF;			//bandera del timer
uint8_t convert_end = OFF;			//bandera del DoADC
uint8_t dato_recibido;
uint8_t dato;
uint16_t adc_buffer[ADC_BUFF_SIZE];

/*==================[internal functions declaration]=========================*/

/* @brief indica la interrupcion para la conversión analógica
 * @param[in] No Parameter
 */
void DoTimer(void);

/* @brief indica la interrupcion para el periferico UART
 * @param[in] No Parameter
 */
void DoUart(void);

/* @brief indica la interrupcion para el ADC
 * @param[in] No Parameter
 */
void DoADC(void);

/* @brief promedio de la señal
 * @param[in] vector de datos y tamaño del vector
 */
 uint8_t Calcular_Promedio(uint16_t *buff, uint8_t tamano);

/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A, 25, &DoTimer};
serial_config my_uart = {SERIAL_PORT_PC, 115200, &DoUart};
analog_input_config my_ADC = {CH1, AINPUTS_SINGLE_READ, &DoADC};

/*==================[external functions definition]==========================*/

void DoTimer(void)
{
	timer_end = ON;
}

void DoUart(void)
{
	UartReadByte(my_uart.port, &dato);				//lee el dato del UART
}

void DoADC(void)		//conversor Analógico-Digital
{
	AnalogInputRead(my_ADC.input, &dato_recibido);		//lee el dato del ADC
	convert_end = ON;
}

uint16_t Calcular_Promedio(uint16_t *buff, uint8_t tamano)
{
	uint32_t acumulador = 0;
	uint8_t i = 0;
	for(i=0; i<tamano; i++)
	{
		acumulador += buff[i];
	}
	uint16_t promedio = acumulador/tamano;

	return promedio;
}

void SysInit(void)					//inicializaciones
{
	SystemClockInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_uart);
	AnalogInputInit(&my_ADC);
	AnalogOutputInit();
}

int main(void)
{
	SysInit();

	uint8_t index_func = 0;
	uint16_t dato_salida = 0;

	while(1)
    {
    	if(timer_end == ON)
    	{
    		AnalogStartConvertion();
    		timer_end = OFF;
    	}

    	if (convert_end == ON)
    	{
    		adc_buffer[index_func] = dato_recibido;
    		index_func++;
    		if (index_func == ADC_BUFF_SIZE)
    		{
    			index_func =0;
    			dato_salida = Calcular_Promedio(adc_buffer, ADC_BUFF_SIZE);
    		}
    	}
    		UartSendString(my_uart.port, UartItoa(dato_salida, 10));
    		UartSendString(my_uart.port, "\r");
    		convert_end = OFF;
   	}
}


/*==================[end of file]============================================*/

