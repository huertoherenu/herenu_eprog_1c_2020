/*
 * hc_sr4.c
 *
 *  Created on: 24 abr. 2020
 *      Author: Huerto Herenu
 *      huerto.herenu@gmail.com
 */


/*==================[inclusions]=============================================*/
#include "../inc/hc_sr4.h"
#include "gpio.h"
#include "systemclock.h"
#include "delay.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/

#define TRIGGER_TIME 10		//tiempo del pulso de trigger (10[us])
#define ECHO_TIME 1			//tiempo de cada pulso echo (1[us])

#define CONSTANT_CM 29		//constante para pasar de tiempo a distancia en cm.
#define CONSTANT_IN 74		//constante para pasar de tiempo a distancia en pulgadas
//Con las constantes dadas en la hoja de datos del sensor obtenía el doble de la distancia que se medía
//por lo cual se modificaron ambas.

gpio_t var_echo;
gpio_t var_trigger;
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

bool HcSr04Init(gpio_t echo, gpio_t trigger)
{
	/** Configuration of the GPIO */
		var_echo = echo;
		var_trigger = trigger;

		GPIOInit(var_echo, GPIO_INPUT);			 // Se utiliza la función GPIOInit(gpio_t pin, io_t io)
		GPIOInit(var_trigger, GPIO_OUTPUT);

		return (true);
}

int16_t HcSr04ReadDistanceCentimeters(void){

	int16_t distance;
	int16_t reading_time=0;

	GPIOOn(var_trigger);
	DelayUs(TRIGGER_TIME);
	GPIOOff(var_trigger);						// Setear el pulso de salida (10[us] según la hoja de datos)

	while(GPIORead(var_echo)==false)
	{
	}										// Esperar hasta que echo pase de '0' a '1'

	while(GPIORead(var_echo)==true)
		{
			DelayUs(ECHO_TIME);
			reading_time++;
		}									// Contar cuantos ciclos tarda echo en pasar de '1' a '0'

	distance = reading_time/CONSTANT_CM;	// Aplicar fórmula para convertir tiempo en distancia [cm]

	return (distance);
}

int16_t HcSr04ReadDistanceInches(void){

	int16_t distance;
	int16_t reading_time=0;

	GPIOOn(var_trigger);
	DelayUs(TRIGGER_TIME);
	GPIOOff(var_trigger);						// Setear el pulso de salida (10[us] según la hoja de datos)

	while(GPIORead(var_echo)==false)
	{
	}										// Esperar hasta que echo pase de '0' a '1'

	while(GPIORead(var_echo)==true)
		{
			DelayUs(ECHO_TIME);
			reading_time++;
		}									// Contar cuantos ciclos tarda echo en pasar de '1' a '0'

	distance = reading_time/CONSTANT_IN;	// Aplicar fórmula para convertir tiempo en distancia [pulgadas].

	return (distance);
}

int16_t HcSr04Readvolumen(void){

	int16_t volumen;
	int16_t reading_time=0;
	int16_t h = 12;				//altura del vaso
	int16_t R = 2;				//radio del vaso

	GPIOOn(var_trigger);
	DelayUs(TRIGGER_TIME);
	GPIOOff(var_trigger);						// Setea el pulso de salida (10[us] según la hoja de datos)

	while(GPIORead(var_echo)==false)
	{
	}										// Esperar hasta que echo pase de '0' a '1'

	while(GPIORead(var_echo)==true)
		{
			DelayUs(ECHO_TIME);
			reading_time++;
		}									// Contar cuantos ciclos tarda echo en pasar de '1' a '0'

	volumen = 3.14*(h-(reading_time/CONSTANT_CM))*R*R;	// 	Fórmula del volumen teniendo en cuenta que la distancia debe convertirse de tiempo a [cm]

	return (volumen);
}

bool HcSr04Deinit(gpio_t echo, gpio_t trigger);

/*==================[end of file]============================================*/
