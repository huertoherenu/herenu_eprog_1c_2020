/*
 * hc_sr4.h
 *
 *  Created on: 24 abr. 2020
 *      Author: Huerto Hereñu
 *      huerto.herenu@gmail.com
 */

#ifndef HC_SR4_H_
#define HC_SR4_H_
/**
 **
 ** This is the driver to measure the proximity of an object by ultrasound
 **
 **
 **/

/*==================[inclusions]=============================================*/
#include "bool.h"
#include "gpio.h"

#include <stdint.h>

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @brief Initialization function of EDU-CIAA HC_SR4
 *
 * Port mapping and initial state of sensor ports
 *
 * @param[in] echo, trigger
 *
 * @return TRUE if no error
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);

/** @brief Function to read the distance of an object in centimeters
 *
 * @param[in] No Parameter
 *
 */
int16_t HcSr04ReadDistanceCentimeters(void);

/** @brief Function to read the distance of an object in inches
 *
 * @param[in] No Parameter
 *
 */
int16_t HcSr04ReadDistanceInches(void);

/** @brief Funcion para calcular el volumen del vaso a partir del sensado de distancia
 *
 * @param[in] No Parameter
 *
 ** @return el valor del volumen del vaso
 */

int16_t HcSr04Readvolumen(void);

/** @brief Deinitialization function of EDU-CIAA HC_SR4
 *
 * @param[in] echo, trigger
 *
 * @return TRUE if no error
 */

bool HcSr04Deinit(gpio_t echo, gpio_t trigger);


/*==================[end of file]============================================*/
#endif /* HC_SR4_H_ */
